package net.thracian.westminster.dicegames.util;

import java.util.Random;

/**
 * @author: Stefan Vatov
 * Date: 10/20/13
 * Time: 11:42 PM
 */
public class RandomGenerator {

    private Random random;
    private static volatile RandomGenerator instance = null;

    private RandomGenerator() {
        random = new Random();
    }

    public static RandomGenerator getInstance() {
        if (null == instance)
            synchronized (RandomGenerator.class) {
                if (null == instance)
                    instance = new RandomGenerator();
            }

        return instance;
    }


    public int getRandom(int upperLimit) {
        return random.nextInt(upperLimit) + 1;
    }
}
