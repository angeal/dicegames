package net.thracian.westminster.dicegames.util.Statistics;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author: Stefan Vatov
 * Date: 10/21/13
 * Time: 12:06 AM
 */
public class ScoreSheet implements Iterable<RoundSheet>{
    private ArrayList<RoundSheet> roundSheets;

    public ScoreSheet() {
        this.roundSheets = new ArrayList<RoundSheet>();
    }

    public ArrayList<RoundSheet> getRoundSheets() {
        return roundSheets;
    }

    @Override
    public Iterator iterator() {
        Iterator<RoundSheet> it = roundSheets.iterator();
        return it;
    }
    public void add(RoundSheet sheet) {
        this.roundSheets.add(sheet);
    }
}
