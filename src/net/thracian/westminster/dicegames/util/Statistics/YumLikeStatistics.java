package net.thracian.westminster.dicegames.util.Statistics;

/**
 * @author: Stefan Vatov
 * Date: 10/21/13
 * Time: 12:36 PM
 */
public class YumLikeStatistics implements IStatistics {

    private ScoreSheet scoreSheet;

    public YumLikeStatistics() {
        this.scoreSheet = new ScoreSheet();
    }

        @Override
    public void calculate() {
        this.show();
    }

    @Override
    public void show() {
        int roundCounter = 1;

        for (RoundSheet roundSheet : scoreSheet) {

            for (PlayerHand playerHand : roundSheet) {
                System.out.print("Round [" + roundCounter + "]" + " Player[" + playerHand.getPlayerId() + "] : ");
                for (Integer currentThrow : playerHand) {
                    System.out.print(" " + currentThrow + " ");
                }

                System.out.print("\n");
            }
            roundCounter++;
        }
    }

    @Override
    public void addRound(RoundSheet sheet) {
        this.scoreSheet.add(sheet);
    }
}
