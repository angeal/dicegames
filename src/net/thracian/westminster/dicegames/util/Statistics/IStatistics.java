package net.thracian.westminster.dicegames.util.Statistics;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * @author: Stefan Vatov
 * Date: 10/15/13
 * Time: 4:06 PM
 */
public interface IStatistics {
    void calculate();
    void show();
    void addRound(RoundSheet sheet);
}
