package net.thracian.westminster.dicegames.util.Statistics;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author: Stefan Vatov
 * Date: 10/21/13
 * Time: 12:18 AM
 */
public class PlayerHand implements Iterable<Integer>{
    private int playerId;
    private ArrayList<Integer> diceThrows;

    public PlayerHand(int playerId, ArrayList<Integer> diceThrows) {
        this.playerId = playerId;
        this.diceThrows = diceThrows;
    }

    public int getPlayerId() {
        return playerId;
    }

    public ArrayList<Integer> getDiceThrows() {
        return diceThrows;
    }

    @Override
    public Iterator<Integer> iterator() {
        return diceThrows.iterator();
    }
}
