package net.thracian.westminster.dicegames.util.Statistics;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author: Stefan Vatov
 * Date: 10/21/13
 * Time: 12:08 AM
 */
public class RoundSheet implements Iterable<PlayerHand>{
    private ArrayList<PlayerHand> playerHands;

    public RoundSheet(ArrayList<PlayerHand> playerHands) {
        this.playerHands = playerHands;
    }

    public RoundSheet() {
        this.playerHands = new ArrayList<PlayerHand>();
    }

    public ArrayList<PlayerHand> getPlayerHands() {
        return playerHands;
    }

    @Override
    public Iterator<PlayerHand> iterator() {
        return playerHands.iterator();
    }

    public int getPlayerCount() {
        return playerHands.size();
    }

    public void clear() {
        this.playerHands.clear();
    }

    public void add(PlayerHand score) {
        this.playerHands.add(score);
    }
}
