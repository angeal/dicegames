package net.thracian.westminster.dicegames.util.Statistics;

import net.thracian.westminster.dicegames.util.IntComparator;

import java.util.*;

/**
 * @author: Stefan Vatov
 * Date: 10/15/13
 * Time: 4:07 PM
 */
public class SimpleGameStatistics implements IStatistics{

    private int pairThrownCount = 0;
    private boolean isPairThrown = false;

    private ScoreSheet gameScores;

    public SimpleGameStatistics() {
        super();
        gameScores = new ScoreSheet();
    }

    private void isPairThrown() {
        isPairThrown = false;
        pairThrownCount = 0;

        for (RoundSheet roundScores : gameScores) {



            for (PlayerHand score : roundScores) {
                Set<Integer> testSet = new TreeSet<Integer>(new IntComparator());
                for (Integer diceThrow : score) {
                    boolean elementAdded = testSet.add(diceThrow);
                    if (false == elementAdded) {
                        pairThrownCount++;
                    }
                }
            }
        }

        if (pairThrownCount > 0)
        {
            isPairThrown = true;
        }

    }


    @Override
    public void calculate() {
        this.isPairThrown();
        System.out.println("This game has had a double thrown: " + this.isPairThrown);
        if (true == this.isPairThrown) {
            System.out.println("Pairs thrown during this game: " + this.pairThrownCount);
        }
    }

    @Override
    public void addRound(RoundSheet sheet) {
        gameScores.add(sheet);
    }

    @Override
    public void show() {
        int roundCounter = 1;

        for (RoundSheet roundSheet : gameScores) {

            for (PlayerHand playerHand : roundSheet) {
                System.out.print("Round [" + roundCounter + "]" + " Player[" + playerHand.getPlayerId() + "] : ");
                for (Integer currentThrow : playerHand) {
                    System.out.print(" " + currentThrow + " ");
                }

                System.out.print("\n");
            }
            roundCounter++;
        }
    }
}
