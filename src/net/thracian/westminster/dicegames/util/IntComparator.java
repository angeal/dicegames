package net.thracian.westminster.dicegames.util;

import java.util.Comparator;

/**
 * @author: Stefan Vatov
 * Date: 10/15/13
 * Time: 4:21 PM
 */
public class IntComparator implements Comparator<Integer> {
    @Override
    public int compare(Integer integer, Integer integer2) {
        return integer.compareTo(integer2);
    }
}
