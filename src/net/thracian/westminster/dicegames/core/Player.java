package net.thracian.westminster.dicegames.core;

import java.util.ArrayList;

/**
 * @author: Stefan Vatov
 * Date: 10/15/13
 * Time: 12:58 AM
 */
public class Player {

    private int playerNumber;

    /**
     * Implements dice throw per round.
     * @param numberOfDice indicates how many dices are thrown per throw.
     * @param dice   instance of Dice
     * @return an ArayList containing the integer values of each dice roll.
     */
    public ArrayList<Integer> throwDice(int numberOfDice, IDice dice)
    {
        ArrayList<Integer> rollResults = new ArrayList<Integer>();
        for (int i = 1; i <= numberOfDice; i++) {
            rollResults.add(dice.roll());
        }

        return rollResults;
    }

    /**
     * Sets the player number (id)
     * @param playerNumber
     */
    protected void setPlayerNumber(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    /**
     * Return the player number.
     * @return  unique player number (id)
     */
    public int getPlayerNumber() {

        return playerNumber;
    }

}
