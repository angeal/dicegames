package net.thracian.westminster.dicegames.core;

import java.util.ArrayList;

/**
 * @author: Stefan Vatov
 * Date: 10/15/13
 * Time: 1:35 AM
 */
public class Table {

    private ArrayList<Player> players;
    private final int DEFAULT_PLAYER_NUMBER = 2;

    public Table(ArrayList<Player> players) {
        this.players = players;
    }

    public Table() {
        this.players = initDefaultPlayers(DEFAULT_PLAYER_NUMBER);
    }

    public Table(int numberOfPlayers) {
        this.players = initDefaultPlayers(numberOfPlayers);
    }

    /**
     * Initializes players per game
     * @param count holds the number of players
     * @return  ArrayList of players
     */
    private ArrayList<Player> initDefaultPlayers(int count) {
        ArrayList<Player> newPlayers = new ArrayList<Player>();
        for (int i=1; i <= count; i++) {
            Player tmpPlayer = new Player();
            tmpPlayer.setPlayerNumber(i);
            newPlayers.add(tmpPlayer);
        }

        return newPlayers;
    }

    public int getPlayerCount() {
        return players.size();
    }

    /**
     * Elicit the winning player.
     * @param winner holds the winning player number
     * @return the winning player
     */
    public Player getPlayerByNumber(int winner) {
        Player winningPlayer = new Player();
        winningPlayer.setPlayerNumber(-1);
        for (Player player : players) {
            if (player.getPlayerNumber() == winner) {
                winningPlayer = player;
            }
        }

        return winningPlayer;
    }

    public ArrayList<Player> players() {
        return new ArrayList<Player>(players);
    }
}
