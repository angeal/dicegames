package net.thracian.westminster.dicegames.core;

import java.util.ArrayList;

/**
 * @author: Stefan Vatov
 * Date: 10/15/13
 * Time: 12:40 AM
 */
public interface IDice {
    int roll();
}
