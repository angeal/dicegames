package net.thracian.westminster.dicegames.core;

import net.thracian.westminster.dicegames.util.RandomGenerator;

import java.util.Random;

/**
 * @author: Stefan Vatov
 * Date: 10/15/13
 * Time: 2:09 AM
 */
public class SixSidedDice implements IDice {

    private final int NUMBER_OF_SIDES = 6;
    private RandomGenerator generator;

    public int getNumberOfSides() {
        return NUMBER_OF_SIDES;

    }

    public SixSidedDice() {
        this.generator = RandomGenerator.getInstance();
    }

    @Override
    /**
     * Implements a roll behaviour of a dice.
     * @return the integer result from a roll.
     */
    public int roll() {
        Random r = new Random();
        int roll = generator.getRandom(NUMBER_OF_SIDES);
        return roll;
    }
}
