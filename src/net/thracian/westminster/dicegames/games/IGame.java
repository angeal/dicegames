package net.thracian.westminster.dicegames.games;

import net.thracian.westminster.dicegames.core.Player;

/**
 * @author: Stefan Vatov
 * Date: 10/15/13
 * Time: 12:37 AM
 */
public interface IGame {

    void showWinner();
    void play();



}
