package net.thracian.westminster.dicegames.games;

import net.thracian.westminster.dicegames.core.Player;
import net.thracian.westminster.dicegames.core.SixSidedDice;
import net.thracian.westminster.dicegames.core.Table;
import net.thracian.westminster.dicegames.util.Statistics.IStatistics;
import net.thracian.westminster.dicegames.util.Statistics.PlayerHand;
import net.thracian.westminster.dicegames.util.Statistics.RoundSheet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author: Stefan Vatov
 * Date: 10/21/13
 * Time: 11:28 AM
 */
public class YumLikeGame implements IGame {

    private final int NUMBER_OF_DICE = 5;
    private final int NUMBER_OF_PLAYERS = 2;

    private Table gameTable;
    private RoundSheet lastRound;
    private IStatistics statistics = null;
    private ArrayList<Integer> yumThrowers;
    private HashMap<Integer, ArrayList<Integer>> highestThrows;




    public YumLikeGame(IStatistics statistics){
        this.gameTable = new Table(NUMBER_OF_PLAYERS);
        this.statistics = statistics;
        this.yumThrowers = new ArrayList<Integer>();
        this.highestThrows = new HashMap<Integer, ArrayList<Integer>>();
    }



    private boolean yumCheck(PlayerHand hand) {
        Integer lastThrow = -1;
        int foundCount = 0;


        lastThrow = hand.getDiceThrows().get(0);
        for(Integer diceThrow : hand) {
            boolean areEqual = diceThrow.equals(lastThrow);
            if (areEqual) {
                foundCount++;
            }
        }

        if (foundCount == 5) {
            this.yumThrowers.add(hand.getPlayerId());
            this.saveHighest(hand, 100);
            return true;
        }

        return false;
    }




    @Override
    public void showWinner() {
        if (0 != yumThrowers.size()) {
            for (Integer playerId : yumThrowers)
                System.out.println("Yum thrown by player [" + playerId + "]");
        } else {
            int winnerId = this.calculateWinnerFromHighest();
            System.out.println("Winner with highest score is player [" + winnerId + "]");
        }



    }

    @Override
    public void play() {

        for (Player player : gameTable.players()) {
            for (int i = 0; i<=2; i++) {
                PlayerHand score = new PlayerHand(player.getPlayerNumber(), player.throwDice(NUMBER_OF_DICE - i, new SixSidedDice()));
                System.out.println("Player [" + player.getPlayerNumber() + "] threw" + score.getDiceThrows().toString());
                if (0 == i)
                {
                    if (yumCheck(score))
                        break;
                } else if (2 == i) {
                    this.sumDice(score);
                } else {
                    this.getHighest(score);
                }
            }
        }

        this.showWinner();
    }

    private void getHighest(PlayerHand score) {
        int max = -1;
        for (Integer diceThrow : score) {
            if (diceThrow > max)
                max = diceThrow;
        }

        saveHighest(score, max);
    }

    private void saveHighest(PlayerHand score, int max) {
        ArrayList<Integer> tmpScores = highestThrows.get(score.getPlayerId());
        if  (null != tmpScores)
        {
            tmpScores.add(max);
            highestThrows.put(score.getPlayerId(), tmpScores);
        } else
        {
            tmpScores = new ArrayList<Integer>();
            tmpScores.add(max);
            highestThrows.put(score.getPlayerId(), tmpScores);
        }
    }

    private void sumDice(PlayerHand hand) {
        int sum = 0;
        for(Integer diceThrow : hand) {
            sum += diceThrow;
        }

        this.saveHighest(hand, sum);
    }

    private int calculateWinnerFromHighest() {
        int[] playerFinalScores = new int[NUMBER_OF_PLAYERS];

        Iterator<Map.Entry<Integer, ArrayList<Integer>>> it = this.highestThrows.entrySet().iterator();

        while(it.hasNext()) {
            Map.Entry<Integer, ArrayList<Integer>> playerScores = it.next();

            int sum = 0;
            for(Integer highScore : playerScores.getValue()) {
                sum += highScore;
            }

            playerFinalScores[playerScores.getKey()-1] = sum;
        }

        int max = -1;
        int winnerId = -1;
        for(int i = 0; i<=NUMBER_OF_PLAYERS-1; i++) {
            if (playerFinalScores[i] > max) {
                max = playerFinalScores[i];
                winnerId = i;
            }
        }

        return winnerId+1;
    }

}
