package net.thracian.westminster.dicegames.games;

import net.thracian.westminster.dicegames.core.Player;
import net.thracian.westminster.dicegames.core.SixSidedDice;
import net.thracian.westminster.dicegames.core.Table;
import net.thracian.westminster.dicegames.util.Statistics.IStatistics;
import net.thracian.westminster.dicegames.util.Statistics.PlayerHand;
import net.thracian.westminster.dicegames.util.Statistics.RoundSheet;

import java.util.*;

/**
 * @author: Stefan Vatov
 * Date: 10/15/13
 * Time: 12:45 AM
 */
public class SimpleDiceGame implements IGame{

    private int numberOfDice = 2;
    private Table gameTable;

    private RoundSheet lastRound;
    private int numberOfRounds = 5;
    private ArrayList<Integer> roundWins;
    private IStatistics statistics = null;


    private void init() {
        this.roundWins = new ArrayList<Integer>();
        this.lastRound = new RoundSheet();
    }

    public SimpleDiceGame(int numberOfDice, Table gameTable, int numberOfRounds, IStatistics statistics) {
        this.numberOfDice = numberOfDice;
        this.gameTable = gameTable;
        this.numberOfRounds = numberOfRounds;
        this.statistics = statistics;
        init();
    }

    public SimpleDiceGame() {
        this.gameTable = new Table();
        init();
    }

    public SimpleDiceGame(int numberOfDice,int numberOfPlayers, int numberOfRounds, IStatistics statistics) {
        this.numberOfDice = numberOfDice;
        this.gameTable = new Table(numberOfPlayers);
        this.numberOfRounds = numberOfRounds;
        this.statistics = statistics;
        init();
    }


    public SimpleDiceGame(int numberOfPlayers, int numberOfRounds, IStatistics statistics) {
        this.gameTable = new Table(numberOfPlayers);
        this.numberOfRounds = numberOfRounds;
        this.statistics = statistics;
        init();
    }

    public SimpleDiceGame(int numberOfPlayers, IStatistics statistics) {
        this.gameTable = new Table(numberOfPlayers);
        this.statistics = statistics;
        init();
    }



    @Override
    public void showWinner() {
        int winner = -1;
        int maxWins = 0;
        Hashtable<Integer, Integer> wins = new Hashtable<Integer, Integer>();

        for (Player player : gameTable.players()) {
            wins.put(player.getPlayerNumber(), 0);
        }

        for (int playerNumber : roundWins) {
            wins.put(playerNumber, wins.get(playerNumber) + 1);
        }

        Iterator<Map.Entry<Integer,Integer>> it = wins.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<Integer, Integer> entry = it.next();
            if (entry.getValue() > maxWins) {
                winner = entry.getKey();
                maxWins = entry.getValue();
            }
        }

        System.out.println("Player " + winner + " has won the game with " + maxWins + " winning rounds!");
    }

    /**
     * Calculate the overall sum of all thrown dices results
     * @param score
     * @return
     */
    private int sumDice(PlayerHand score) {

        int sum = 0;

        for (Integer roll : score) {
            sum += roll;
        }

        return sum;
    }

    /**
     * Initialize each game round and add the round winner number to an integer ArrayList.
     */
    private void nextRound() {

        lastRound = new RoundSheet();

        for (Player player : gameTable.players()) {
            PlayerHand score = new PlayerHand(player.getPlayerNumber(), player.throwDice(numberOfDice, new SixSidedDice()));

            lastRound.add(score);
        }
        statistics.addRound(lastRound);

        roundWins.add(roundWinner().getPlayerNumber());
    }


    @Override
    public void play() {
        System.out.println("-------------------------------");
        System.out.println("Game starts");
        System.out.println("-------------------------------");
        for (int i = 1; i <= numberOfRounds; i++) {
            System.out.println("Round " + i + " starts !  ----- |");
            nextRound();
            System.out.println("Round " + i + " has finished.");
        }

        showWinner();
        if (null != statistics) {
            statistics.calculate();
            statistics.show();
        }
    }

    /**
     * Elicit the winner for each game round.
     * @return winning player number.
     */
    private Player roundWinner() {
        int winner = -1;
        int biggestSum = 0;

        for(PlayerHand score : lastRound) {
           int tmpSum = sumDice(score);
           if (tmpSum > biggestSum) {
               biggestSum = tmpSum;
               winner = score.getPlayerId();
           }
        }
        System.out.println("Round winner is " + winner);
        return gameTable.getPlayerByNumber(winner);
    }
}
