package net.thracian.westminster.dicegames.run;

import net.thracian.westminster.dicegames.games.YumLikeGame;
import net.thracian.westminster.dicegames.util.Statistics.IStatistics;
import net.thracian.westminster.dicegames.util.Statistics.SimpleGameStatistics;

/**
 * @author: Stefan Vatov
 * Date: 10/15/13
 * Time: 2:18 AM
 */
public class GameTest {

    public static void main(String[] args) {
        IStatistics statistics = new SimpleGameStatistics();
        for (int i=0; i<=1000; i++) {
            YumLikeGame testGame = new YumLikeGame(statistics);
            testGame.play();
        }
    }
}
